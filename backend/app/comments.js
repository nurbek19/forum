const express = require('express');
const router = express.Router();
const Comment = require('../models/Comment');
const User = require('../models/User');

const createRouter = () => {
    router.get('/:id', (req, res) => {
        Comment.find({postId: req.params.id}).populate('user postId')
            .then(comments => res.send(comments))
            .catch(error => res.status(400).send(error));
    });

    router.post('/', async (req, res) => {
        const token = req.get('Token');
        const comment = req.body;

        if (!token) {
            return res.status(401).send({error: 'Token is not found'});
        }

        const user = await User.findOne({token: token});

        if (!user) {
            return res.status(401).send({error: 'User is not authorized'});
        }

        comment.user = user._id;
        const c = new Comment(comment);

        await c.save();

        return res.send(c);
    });

    return router;
};

module.exports = createRouter;