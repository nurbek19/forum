import React from 'react';
import notFound from '../../assets/images/not-found.png';
import {Image, Panel} from "react-bootstrap";
import {Link} from "react-router-dom";

const ArtistList = props => {
    let image = notFound;

    if (props.image) {
        image = 'http://localhost:8000/uploads/' + props.image;
    }

    return (
        <Panel>
            <Panel.Body>
                <Image
                    style={{width: '150px', marginRight: '10px'}}
                    src={image}
                    thumbnail
                />
                <h5>{props.datetime} by {props.user}</h5>
                <Link to={'/post/' + props.id}>
                    {props.title}
                </Link>
            </Panel.Body>
        </Panel>
    )
};

export default ArtistList;
