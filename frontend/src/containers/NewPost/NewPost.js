import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, Col, Form, FormGroup, PageHeader} from "react-bootstrap";
import FormElement from "../../components/UI/Form/FormElement";
import {createPost} from "../../store/actions/posts";

class Register extends Component {
    state = {
        title: '',
        description: '',
        image: ''
    };

    inputChangeHandler = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key])
        });

        this.props.onSubmit(formData, this.props.user.token);
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    render() {
        return (
            <Fragment>
                <PageHeader>Create new post</PageHeader>
                <Form horizontal onSubmit={this.submitFormHandler}>
                    <FormElement
                        propertyName="title"
                        title="Title"
                        placeholder="title"
                        autoComplete="new-title"
                        type="text"
                        value={this.state.title}
                        changeHandler={this.inputChangeHandler}
                        required
                    />

                    <FormElement
                        propertyName="description"
                        title="Description"
                        placeholder="description ..."
                        autoComplete="new-description"
                        type="text"
                        value={this.state.description}
                        changeHandler={this.inputChangeHandler}
                        error={this.props.error && this.props.error}
                    />

                    <FormElement
                        propertyName="image"
                        title="Image"
                        type="file"
                        changeHandler={this.fileChangeHandler}
                        error={this.props.error && this.props.error}
                    />

                    <FormGroup>
                        <Col smOffset={2} sm={10}>
                            <Button
                                bsStyle="primary"
                                type="submit"
                            >Create post</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    error: state.posts.error
});

const mapDispatchToProps = dispatch => ({
    onSubmit: (formData, token) => dispatch(createPost(formData, token))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);