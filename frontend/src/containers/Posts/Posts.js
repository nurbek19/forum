import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import PostList from "../../components/PostList/PostList";
import {PageHeader} from "react-bootstrap";
import {getPosts} from "../../store/actions/posts";

class Artists extends Component {
    componentDidMount() {
        this.props.getPosts();
    }

    render() {
        return (
            <Fragment>
                <PageHeader>
                    Posts
                </PageHeader>
                {this.props.posts.map(post => (
                    <PostList key={post._id} id={post._id} title={post.title} image={post.image}
                              datetime={post.datetime} user={post.user.username}/>
                ))}
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    posts: state.posts.posts
});

const mapDispatchToProps = dispatch => ({
    getPosts: () => dispatch(getPosts())
});

export default connect(mapStateToProps, mapDispatchToProps)(Artists);
